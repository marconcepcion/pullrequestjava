package aytocalc;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MultiplicatePorZeroTest {

	@Test
	void testMultiplicatePorZero() {
		MultiplicatePorZero calculadora = new MultiplicatePorZero();
	    double resultado =calculadora.calcula(100.0);			
	    assertEquals(0, resultado);	
	    //esta ok!!
	}

}
